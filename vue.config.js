const path = require('path')

function resolve (dir) {
    return path.join(__dirname, dir)
}

module.exports = {
    css: {
        loaderOptions: {
            stylus: {
                'resolve url': true,
                'import': [
                    './src/theme',
                    './src/common/stylus/variable.styl',
                    './src/common/stylus/mixin.styl'
                ]
            }
        }
    },
    pluginOptions: {
        'cube-ui': {
            postCompile: true,
            theme: true
        }
    },
    chainWebpack (config) {
        // 文件别名
        config.resolve.alias
            .set('components', resolve('src/components'))
            .set('common', resolve('src/common'))
            .set('api', resolve('src/api'))
    },
    productionSourceMap: false,
    devServer: {
        // proxy: {
        //     '/api': {
        //         target: '<url>',
        //         changeOrigin: true
        //     }
        // }
    }
}
